from django.apps import AppConfig


class CaptchaapiConfig(AppConfig):
    name = 'CaptchaAPI'
