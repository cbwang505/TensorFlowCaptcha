import cv2
import numpy as np
import urllib
from matplotlib import pyplot as plt

# img9 = cv2.imread('test/c1.png', 0)
img5 = cv2.imread('test/c1.png')

GrayImage = cv2.cvtColor(img5, cv2.COLOR_BGR2GRAY)

tmp = cv2.medianBlur(GrayImage, 1)

# ret2, zh = cv2.threshold(GrayImage,  170, 255, cv2.THRESH_BINARY_INV)

ret, img = cv2.threshold(tmp, 170, 255, cv2.THRESH_BINARY_INV)


kernel = np.ones((2, 2), np.uint8)

img = cv2.dilate(img, kernel, iterations=1)

img = cv2.erode(img, kernel, iterations=1)
img = cv2.dilate(img, kernel, iterations=1)

img = cv2.erode(img, kernel, iterations=1)
img = cv2.medianBlur(img, 1)
img = cv2.dilate(img, kernel, iterations=1)

# medianBlur for removing salt and pepper noise

ret, img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)

ret, thresh1 = cv2.threshold(GrayImage, 255, 127, cv2.THRESH_BINARY)
ret, thresh2 = cv2.threshold(GrayImage, 170, 255, cv2.THRESH_BINARY_INV)
ret, thresh3 = cv2.threshold(GrayImage, 127, 255, cv2.THRESH_TRUNC)
ret, thresh4 = cv2.threshold(GrayImage, 170, 0, cv2.THRESH_TOZERO)
ret, thresh5 = cv2.threshold(GrayImage, 170, 255, cv2.THRESH_TOZERO_INV)
titles = ['Gray Image', 'BINARY', 'BINARY_INV', 'TRUNC', 'TOZERO', 'TOZERO_INV']
images = [GrayImage, thresh1, thresh2, thresh3, thresh4, thresh5]
for i in range(6):
    plt.subplot(2, 3, i + 1), plt.imshow(images[i], 'gray')
    plt.title(titles[i])
    plt.xticks([]), plt.yticks([])
plt.show()
